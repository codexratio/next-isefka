let messagesFR = {
  Save: "Sauvegarder",
  Validate: "Valider",
  Cancel: "Annuler",
  "Follow us on Twitter": "Suivez-nous sur Twitter",
  "Follow us on Facebook": "Suivez-nous sur Facebook",
  Show: "Voir",
  Download: "Télécharger",
  "Convert JSON to CSV": "Convertir du JSON en CSV",
  login: "connexion",
  "sign up": "inscription",
  "JSON file(s)": "Fichier(s) JSON",
  "No file received": "Aucun fichier reçu",
  "Record screen online": "Filmer son écran",
  "Instant recording of screen":
    "Appuyer sur le bouton Enregistrer pour filmer votre écran.",
  Record: "Démarrer l'enregistrement",
  "The verification code is invalid.": "Le code de vérification est invalide.",
  "Please type the verification code":
    "Veuillez saisir le code de vérification",
  "We sent a verification code to your email. Please enter the verification code below.":
    "Nous avons envoyé un code de vérification sur votre boite email. Veuillez entrer le code de vérification ci-dessous afin de vérifier votre adresse email.",
  "Your verification code": "Votre code de vérification",
  Share: "Partager",
  "We just sent a verification code to your email.":
    "Nous avons envoyé un code de vérification sur votre email.",
  Name: "Nom",
  Type: "Type",
  yes: "oui",
  no: "non",
  "My account": "Mon compte",
  "Try it": "Essayer",
  "An error occured. Please check your request or send an email to support@ikalas.com":
    "Une erreur est survenue. Veuillez vérifier vos entrées ou nous contacter support@ikalas.com",
  "Word to PPT": "Word vers PowerPoint",
  "Unlock PDF": "Débloquer un PDF",
  "Translate text": "Traduire un texte",
  Login: "Se connecter",
  "Your email": "Votre email",
  "Please enter a valid email address.":
    "Veuillez entrer une adresse email valide.",
  "Create a password": "Ajouter un mot de passe",
  "Enter your password": "Entrer votre mot de passe",
  "Please enter a valid password.": "Veuillez entrer un mot de passe valide",
  "You doesn't have an account ?": "Vous n'avez pas de compte ?",
  Signup: "Inscription",
  "I forgot my password": "Mot de passe oublié",
  "Invalid form. Please check your entries.":
    "Formulaire invalide. Des champs n'ont pas été complétés correctement",
  "Confirm your password": "Confirmer le mot de passe",
  "Sign up": "Inscription",
  "Sign in": "Se connecter",
  FR: "FR",
  English: "Anglais",
  French: "Français",
  Arabic: "Arabe",
  "Translate text in image with OCR":
    "Traduire le texte d'une image avec l'OCR/ROC",
  "Translate text extracted from image":
    "Traduire le texte extrait à partir d'une image",
  "Translate text.": "Traduire instantanément un texte.",
  "WMA to MP3": "WMA vers MP3",
  "Convert WMA file to MP3 file": "Convertir un fichier WMA vers MP3",
  "PDF to images": "Convertir un fichier PDF en images",
  "Convert PDF to images": "Convertir un document PDF images",
  "Convert JSON file to CSV file": "Convertir un fichier JSON en fichier CSV",
  "JSON to CSV": "JSON vers CSV",
  "Record audio online": "Enregistrer sa voix en ligne",
  "Instant recording of audio": "Enregistrer sa voix",
  "QR Code": "Code QR",
  "Generate QR Code": "Générer un code QR",
  "Merge PDF files": "Fusionner plusieurs PDF",
  "Merge multiple PDF files in one PDF document":
    "Fusionner plusieurs documents PDF en un seul document",
  "Sublimate an image": "Sublimez une image",
  "in grayscale": "en niveaux de gris",
  // "Donnez de la profondeur ou de la gravité à vos images en transformant leur couleur en noir et blanc" : "",
  "Go to": "Aller",
  "Upload your image": "Envoyer votre image",
  "Get the result": "Obtenir le résultat",
  Userspace: "Mon espace",
  Register: "Inscription",
  "Help center": "Aide",
  Developers: "Développeurs",
  Documentation: "Documentation",
  Company: "Compagnie",
  About: "À propos",
  Services: "Services",
  Contact: "Contact",
  Careers: "Carrières",
  "Sign Up": "Inscription",
  "Sign In": "Se connecter",
  Oui: "Yes",
  Genéral: "General",
  answers: "réponses",
  "Is it free ?": "Est-ce que c'est gratuit ?",
  "Updated today": "",
  "Comment s'inscrire sur le site ?": "",
  "Updated today": "",
  "Les inscriptions ne sont pas encore ouvertes mais le seront bientôt. Si vous souhaitez être notifiés lorsque les inscriptions seront ouverts, envoyez nous un email à contact@ikalas.com":
    "",
  "Pendant combien de temps mes fichiers restent sur Ikalas ?": "",
  "Updated today": "Mis à jour aujourd'hui",
  "Unlock PDF encrypted with password":
    "Débloquer un PDF protégé par un mot de passe",
  "Convert JPEG to PNG": "Convertir une image JPEG en image PNG",
  "Convert JPEG to GIF": "Convertir une image JPEG en image GIF",
  Grayscale: "Image en niveaux de gris",
  "Convert color image to grayscale": "Convertir une image en niveaux de gris",
  "Add frame to picture": "Ajouter une bordure à une image",
  "MP3 to WMA": "MP3 vers WMA",
  "Convert MP3 to WMA": "Convertir un fichier MP3 vers WMA",
  "AVI to MP4": "AVI vers MP4",
  "Ikalas is in beta version. This means that there are possibly more bugs than in a normal version. If you find any bug or you want more information, contact us on contact@ikalas.com":
    "Ikalas est en version bêta. Cela signifie qu'il y a possiblement plus de bugs que dans une version normale. Si vous en trouvez ou si vous voulez plus d'informations, contactez nous sur contact@ikalas.com",
  "Resize image": "Redimensionner une image",
  "Upload file": "Partager un fichier",
  "Upload file and get a link to share":
    "Télécharger un fichier et récupérer un lien pour le partager",
  "File(s)": "Fichier(s)",
  File: "Fichier",
  "Your video is ready. Click here to download it.":
    "Votre vidéo est prête. Cliquer ici pour la télécharger.",
  "Use this link to share your video":
    "Utiliser ce lien pour partager votre vidéo",
  "Analysis and specifications": "Analyse et spécifications",
  "We collect your needs in order to compile a list of specifications ready to be developed. Our analysis takes into account your existing situation, your market, your sector of activity and a permanent watch in order to achieve the best technical implementation.":
    "Nous receuillons vos besoins et ceux de vos utilisateurs afin de dresser une liste de spécifications prêtes à être développées. Notre analyse prend en compte votre existant, votre marché, votre secteur d'activité et une veille permanente afin d'aboutir à la meilleure implémentation technique.",
  Development: "Développement",
  "We use agile methods and best practices to quickly deliver secure, reliable and robust code. Our technological choices are tailor-made to your needs.":
    "Nous utilisons les méthodes agiles et les meilleures pratiques pour livrer rapidement du code sûr, fiable et robuste. Nos choix technologiques sont adaptés sur-mesure à vos besoins.",
  Tests: "Tests",
  "We help you get the best user experience, by relying on a quality test and analysis plan that we develop completely, using the most innovative tools.":
    "Nous vous aidons à obtenir la meilleure expérience utilisateur, en misant sur un plan de test et d'analyse qualité que nous élaborons complètement, en utilisant les outils les plus innovants.",
  Deployment: "Déploiement",
  "With us, the deployment of your solutions is fully supported. Whether in the cloud, on your servers or on our servers, you press a button and all your applications are instantly available and updated.":
    "Avec nous, le déploiement de vos solutions est entièrement pris en charge. Que ce soit sur le cloud, sur vos serveurs ou sur nos serveurs, vous appuyez sur un bouton et toutes vos applications sont instantanément disponibles et mises à jour.",
  "Our teams are available 24 hours a day, 7 days a week to monitor your applications. With a response time of less than 30 minutes, to deal with any anomaly that may arise on your services.":
    "Nos équipes sont disponibles 24h/24, 7j/7 pour veiller sur vos applications. Avec un temps de réponse inférieur à 30 minutes, pour prendre en charge toute anomalie qui surviendrait sur vos services.",
  "Our team helps you monitor all your environments to know your vulnerabilities, before your users. We set up centralized logs that allow you to know the status of your services in the blink of an eye.":
    "Notre équipe vous aide à surveiller tous vos environnements pour connaitre vos failles, avant vos utilisateurs. Nous mettons en place des logs centralisés qui permettent de connaître l'état de vos services en un clin d'oeil.",
  "Our developers are passionate professionals who take care of our clients' code.":
    "Nos développeurs sont des professionnels passionnés qui prennent soin du code de nos clients.",
  "We have strong expertise on several stacks. Here are some of the technologies we often work with:":
    "",
  "We rely on strong values": "Nous nous appuyons sur des valeurs fortes",
  "You want to know more ?": "Vous souhaitez en savoir plus ?",
  "Innovation, success, team, loyalty: these are more than words for us. These are values ​​that drive us to be better.":
    "Innovation, succès, équipe, fidélité: ce sont plus que des mots pour nous. Ce sont des valeurs qui nous poussent à être meilleurs.",
  "We believe that every business must constantly find the best way to carry out its mission.":
    "Nous croyons que chaque entreprise doit en permanence trouver le meilleur moyen d'éxecuter sa mission.",
  "We seek success in all our actions, for all our customers, all our partners and all our teams.":
    "Nous cherchons le succès dans toutes nos actions, pour tous nos clients, tous nos partenaires et toutes nos équipes.",
  "We act with our clients as if we were in the same team, adhering to their values ​​and helping them in their missions.":
    "Nous agissons avec nos clients comme si nous étions dans la même équipe, en adhérant à leurs valeurs et en les aidant dans leurs missions.",
  "We believe that long and stable business relationships are successful for our clients and for us. That is why we aim to always keep the trust of our customers.":
    "Nous pensons que les relations d'affaires longues et stables sont porteuses de succès pour nos clients et pour nous. C'est pourquoi nous visons à toujours garder la confiance de nos clients.",
  "Contact us": "Contactez-nous",
  "We’re here to help.": "Nous sommes là pour vous aider.",
  "Your message was sent.": "Votre message a été envoyé.",
  "Thank you": "Merci",
  "Accessible innovation": "L'innovation accessible",
  "We help you provide the best service to your customers.":
    "Nous vous aidons à fournir le meilleur service à vos clients.",
  "We are a seasoned team that can take charge of your IT developments.":
    "Nous sommes une équipe aguerrie qui peut prendre en charge vos développements informatiques.",
  "We love code": "Nous aimons coder",
  "We have strong expertise on several stacks. Here are some of the technologies we often work with:":
    "Nous avons une forte expertise sur plusieurs stacks. Voici quelques technologies avec lesquelles nous travaillons souvent:",
  "Ikalas offers you tools to get more efficient.":
    "Ikalas vous offre des outils pour être plus efficace.",
  "Follow us on social media": "Suivez-nous sur les réseaux sociaux",
  "Login with Google": "Se connecter avec Google",
  MB: "Mo",
  "maximum per file": "maximum par fichier",
  "free during beta version": "gratuit ",
  "This field is required.": "Ce champ est obligatoire.",
  "This field is invalid.": "Ce champ est invalide.",
  "Ikalas helps you to be more productive":
    "Ikalas est une application web qui aide à simplifier des opérations courantes en ligne, comme la conversion de documents ou d'images",
  "Convert video to GIF": "Convertir une vidéo en GIF",
  to: "vers",
  "Convert WEBM video to MP4 video": "Convertir une vidéo WEBM en MP4",
  "Convert WEBM video to AVI video": "Convertir une vidéo WEBM en AVI",
  "We start with your needs and deliver a full personalized solution.":
    "Nous partons de vos besoins pour vous livrer une solution personnalisée",
  "Let us hear from you directly": "Nous sommes à votre écoute",
  "We always want to hear from you. Let us know how we can best help you and we'll do our very best.":
    "",
  "Full name": "Nom",
  "What can we help you with ?": "En quoi pouvons-nous vous aider ?",
  "Send message": "Envoyer",
  "Get a share link after recording video":
    "Obtenir un lien de partage de votre vidéo",
  Password: "Mot de passe",
  "You must be authenticated to use this feature.":
    "Vous devez être authentifié pour utiliser cette fonctionnalité.",
  Search: "Rechercher",
  "Remove audio from video": "Enlever le son d'une vidéo",
  "": "",
  "Want to work with us?": "Want to work with us?",
  "We're always looking to hire talented folks to join our ever-growing team of designers, engineers, and support staff.":
    "We're always looking to hire talented folks to join our ever-growing team of designers, engineers, and support staff.",
  "View open positions": "View open positions",
  Values: "Values",
  "We care about everyone,": "We care about everyone,",
  "but employees are first": "but employees are first",
  " No matter your job title or department, if you're a member of our team you are our top priority. We can deeply about everyone who works with us.":
    " No matter your job title or department, if you're a member of our team you are our top priority. We can deeply about everyone who works with us.",
  Benefits: "Benefits",
  "We'll take of you with": "We'll take of you with",
  "a great benefits package": "a great benefits package",
  "We put our money where our mouthes are. Once you're part of our team, we're going to take the best possible care of you with tons of benefits and perks unavailable anywhere else.":
    "We put our money where our mouthes are. Once you're part of our team, we're going to take the best possible care of you with tons of benefits and perks unavailable anywhere else.",
  " Comprehensive benefits": " Comprehensive benefits",
  " Health, dental, vision, 401k, and more.":
    " Health, dental, vision, 401k, and more.",
  " Unlimited time off": " Unlimited time off",
  " Vacation on your own terms.": " Vacation on your own terms.",
  " Cutting edge hardware": " Cutting edge hardware",
  " We provide brand new computers and phones.":
    " We provide brand new computers and phones.",
  " Moving assistance": " Moving assistance",
  " We will help you get here to work with us!":
    " We will help you get here to work with us!",
  Applying: "Applying",
  "Let’s find you an": "Let’s find you an",
  "open position": "open position",
  " Find the right job for you no matter what it is that you do.":
    " Find the right job for you no matter what it is that you do.",
  Roles: "Roles",
  "All roles": "All roles",
  Design: "Design",
  Engineering: "Engineering",
  Product: "Product",
  Testing: "Testing",
  Support: "Support",
  Team: "Team",
  "All teams": "All teams",
  Consumer: "Consumer",
  Consulting: "Consulting",
  "Internal tools": "Internal tools",
  Location: "Location",
  "All locations": "All locations",
  London: "London",
  "Los Angeles": "Los Angeles",
  Paris: "Paris",
  "San Francisco": "San Francisco",
  "Design & UX": "Design & UX",
  " User experience and design are top priorities at Landkit.":
    " User experience and design are top priorities at Landkit.",
  "4 openings": "4 openings",
  Role: "Role",
  Team: "Team",
  Location: "Location",
  "Senior UX Designer": "Senior UX Designer",
  "Responsible for design systems and brand management.":
    "Responsible for design systems and brand management.",
  "Motion Designer": "Motion Designer",
  " Responsible for creating life in our apps.":
    " Responsible for creating life in our apps.",
  Product: "Product",
  "San Francisco, CA": "San Francisco, CA",
  "Design Researcher": "Design Researcher",
  "Help us make the best decisions with qualitative experiments.":
    "Help us make the best decisions with qualitative experiments.",
  Consulting: "Consulting",
  London: "London",
  "Production Designer": "Production Designer",
  "Create, collect, and distribute beautiful assets.":
    "Create, collect, and distribute beautiful assets.",
  "This is Landkit's bread and butter – help us make it better.":
    "This is Landkit's bread and butter – help us make it better.",
  "4 openings": "4 openings",
  "Ruby Engineer": "Ruby Engineer",
  "Responsible for pipeline and build system.":
    "Responsible for pipeline and build system.",
  "Javascript Prototyper": "Javascript Prototyper",
  "Helping us build quick experiments for testing.":
    "Helping us build quick experiments for testing.",
  "Reliability Engineer": "Reliability Engineer",
  "Managing operations and testing for improved stability.":
    "Managing operations and testing for improved stability.",
  "Junior PHP Engineer": "Junior PHP Engineer",
  "Help us with any odds and ends that need tackling.":
    "Help us with any odds and ends that need tackling.",
  "Don’t see the job you want?": "Don’t see the job you want?",
  "Let us know": "Let us know",
  "Remember, our employees are our biggest fans.":
    "Remember, our employees are our biggest fans.",
  "No matter your job title or department, if you're a member of our team you are our top priority. We can deeply about everyone who works with us.":
    "No matter your job title or department, if you're a member of our team you are our top priority. We can deeply about everyone who works with us.",
  "Working at Landkit has been a dream. The team is amazing, the office is perfect, and I feel like I'm part of a family. I'll be here for a great many years.":
    "Working at Landkit has been a dream. The team is amazing, the office is perfect, and I feel like I'm part of a family. I'll be here for a great many years.",
  "William Callan": "William Callan"
};

module.exports = messagesFR;
