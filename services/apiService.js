import axios from "axios";

let http = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_URL,
    headers: {
        "Content-type": "application/json",
        "x-rapidapi-proxy-secret": "2a417700-017b-11ea-8640-ad8579c41490"
        // "Authorization": `Bearer ${localStorage.getItem('jwtToken')}`
    }
});


const postFunction = (nameFunction, data) => {
    return http.post("/api/"+nameFunction, data);
};

const postContact = (data) => {
  return http.post("/contact", data);
};

const getFunctionsList = () => http.get("/next/functions");

export default {
    postFunction,
    postContact,
    getFunctionsList
};
