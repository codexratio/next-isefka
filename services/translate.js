import messagesFR from "../config/messagesFR";

const _tr = (englishMessage) => {
  let locale = "en";

  if (typeof window !== "undefined") {
    locale =
      document.URL.search(location.origin + "/fr") >= 0
        ? "fr"
        : document.URL.search(location.origin + "/ar") >= 0
        ? "ar"
        : "en";
  }

  switch (locale) {
    case "en":
      return englishMessage;
    case "fr":
      return messagesFR[englishMessage];
    default:
      return englishMessage;
  }
};

export { _tr };

