import { _tr } from "../services/translate";

const steps = [
  {
    title: _tr("Analysis and specifications"),
    description: _tr(
      "We collect your needs in order to compile a list of specifications ready to be developed. Our analysis takes into account your existing situation, your market, your sector of activity and a permanent watch in order to achieve the best technical implementation."
    )
  },
  {
    title: _tr("Development"),
    description: _tr(
      "We use agile methods and best practices to quickly deliver secure, reliable and robust code. Our technological choices are tailor-made to your needs."
    )
  },
  {
    title: _tr("Tests"),
    description: _tr(
      "We help you get the best user experience, by relying on a quality test and analysis plan that we develop completely, using the most innovative tools."
    )
  },
  {
    title: _tr("Deployment"),
    description: _tr(
      "With us, the deployment of your solutions is fully supported. Whether in the cloud, on your servers or on our servers, you press a button and all your applications are instantly available and updated."
    )
  },
  {
    title: _tr("Support"),
    description: _tr(
      "Our teams are available 24 hours a day, 7 days a week to monitor your applications. With a response time of less than 30 minutes, to deal with any anomaly that may arise on your services."
    )
  },
  {
    title: _tr("Monitoring"),
    description: _tr(
      "Our team helps you monitor all your environments to know your vulnerabilities, before your users. We set up centralized logs that allow you to know the status of your services in the blink of an eye."
    )
  }
];

export default steps;
