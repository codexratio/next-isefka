import { _tr } from "../services/translate";

const technologies = [
  _tr(".NET"),
  _tr("Node.js"),
  _tr("React.js"),
  _tr("PostgreSQL"),
  _tr("SQL Server"),
  _tr("MongoDB"),
  _tr("PHP Symfony"),
  _tr("Wordpress")
];

export default technologies;
