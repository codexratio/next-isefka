import { _tr } from "../services/translate";
import Documentation from "../components/documentation/home";
import ApiService from "../services/apiService";

export default function Doc({ functionsList }) {
  return <Documentation functionsList={functionsList} />;
}

export async function getStaticProps() {
  const res = await ApiService.getFunctionsList();
  const functionsList = res.data;
  return {
    props: {
      functionsList
    }
  };
}
