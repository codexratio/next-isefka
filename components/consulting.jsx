import { _tr } from "../services/translate";
import Image from "next/image";
import Card from "./card";
import Technologie from "./technologie";
import steps from "../data/steps";
import technologies from "../data/technologies";

export default function Consulting() {
  return (
    <>
      <section className="py-md-14 overlay overlay-black overlay-50 landing-section">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 text-center">
              <h1 className="display-2 font-weight-bold text-white">
                {_tr("Accessible innovation")}
              </h1>

              <p className="lead text-white mb-8">
                {_tr("We help you provide the best service to your customers.")}
              </p>
            </div>
          </div>
        </div>
      </section>

      <div className="section pt-10  pb-8 ">
        <div className="container">
          <div className="row justify-content-between align-items-center mb-5">
            <div className="col-12 col-md-4 order-md-2 text-center">
              {/* Image */}
              <img
                className="img-fluid w-75 w-md-100 mb-6 mb-md-0"
                layout="fill"
                src="/img/illustrations/illustration-1-dark.png"
                unoptimized={true}
                alt="..."
              />
            </div>
            <div className="col-12 col-md-7 order-md-1">
              {/* Heading */}
              <h2 className="max-width-450">
                {_tr(
                  "We start with your needs and deliver a full personalized solution."
                )}
              </h2>

              {/* Text */}
              <div className="lead text-muted-90 mb-0">
                {_tr(
                  "We are a seasoned team that can take charge of your IT developments."
                )}
              </div>
            </div>
          </div>{" "}
          {/* / .row */}
          <div className="row">
            <Card {...steps[0]} idx={0} />
            <Card {...steps[1]} idx={1} />
            <Card {...steps[2]} idx={2} />
          </div>
          <div className="row mb-10">
            <Card {...steps[3]} idx={3} />
            <Card {...steps[4]} idx={4} />
            <Card {...steps[5]} idx={5} />
          </div>
        </div>{" "}
        {/* / .container*/}
      </div>

      {/* ABOUT
  ================================================== */}
      <section>
        <div className="container">
          <div className="row justify-content-between align-items-center">
            <div className="col-12 col-md-6 col-lg-5 order-md-2">
              {/* Image grid */}
              <div className="d-flex flex-column w-md-130 mb-6 mb-md-0 hide-if-under-800">
                {/* Image */}
                <img
                  className="img-fluid rounded shadow align-self-start consulting-img1"
                  layout="fill"
                  data-jarallax-element="-20"
                  src="/img/brands/logomark/nodejs.png"
                  alt="..."
                />

                {/* Image */}
                <img
                  className="img-fluid rounded shadow align-self-start consulting-img2"
                  layout="fill"
                  data-jarallax-element="-50"
                  src="/img/brands/logomark/postgresql.png"
                  alt="..."
                />

                {/* Image */}
                <img
                  className=" img-fluid rounded shadow align-self-center consulting-img3"
                  layout="fill"
                  data-jarallax-element="-80"
                  src="/img/brands/logomark/react.png"
                  alt="..."
                />
                {/* Image */}
                <img
                  className=" img-fluid rounded shadow align-self-center consulting-img4"
                  layout="fill"
                  data-jarallax-element="-80"
                  src="/img/brands/logomark/csharp.jpeg"
                  alt="..."
                />
              </div>
            </div>
            <div className="col-12 col-md-6 order-md-1 pt-md-12">
              {/* Heading */}
              <h1>{_tr("We love code")}</h1>
              <p className="font-size-lg text-gray-700 mb-6">
                {_tr(
                  "Our developers are passionate professionals who take care of our clients' code."
                )}
                <br />
                {_tr(
                  "We have strong expertise on several stacks. Here are some of the technologies we often work with:"
                )}
              </p>
              <div className="row">
                <div className="col-12 col-lg-6">
                  <Technologie name={technologies[0]} />
                  <Technologie name={technologies[1]} />
                </div>
                <div className="col-12 col-lg-6">
                  <Technologie name={technologies[2]} />
                  <Technologie name={technologies[3]} />
                </div>
              </div>{" "}
              <div className="row pt-4">
                <div className="col-12 col-lg-6">
                  <Technologie name={technologies[4]} />
                  <Technologie name={technologies[5]} />
                </div>
                <div className="col-12 col-lg-6">
                  <Technologie name={technologies[6]} />
                  <Technologie name={technologies[7]} />
                </div>
              </div>
            </div>
          </div>{" "}
        </div>{" "}
      </section>

      {/* DOWNLOAD
  ================================================== */}
      <section className="border-top mt-10">
        <div className="container">
          <div className="row justify-content-between align-items-stretch">
            <div className="col-12 col-md-6 py-8 py-md-13">
              {/* Heading */}
              <h2>{_tr("We rely on strong values")}</h2>

              {/* Text */}
              <p className="font-size-lg text-gray-700 mb-7">
                {_tr(
                  "Innovation, success, team, loyalty: these are more than words for us. These are values ​​that drive us to be better. "
                )}
                {_tr(
                  "We believe that every business must constantly find the best way to carry out its mission. "
                )}
                {_tr(
                  "We seek success in all our actions, for all our customers, all our partners and all our teams. "
                )}
                {_tr(
                  "We act with our clients as if we were in the same team, adhering to their values ​​and helping them in their missions. "
                )}
                {_tr(
                  "We believe that long and stable business relationships are successful for our clients and for us. That is why we aim to always keep the trust of our customers."
                )}
              </p>
            </div>
            <div className="col-12 col-md-5">
              {/* Image */}
              <div className="position-relative h-100 vw-50 bg-cover cover-team">
                {/* Shape */}
                <div className="shape shape-left shape-fluid-y svg-shim text-white">
                  <svg
                    viewBox="0 0 100 1544"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0 0h100v386l-50 772v386H0V0z"
                      fill="currentColor"
                    />
                  </svg>
                </div>
              </div>
            </div>
          </div>{" "}
          {/* / .row */}
        </div>{" "}
        {/* / .container */}
      </section>

      {/* CTA
  ================================================== */}
      <section className="bg-black">
        <div className="container py-6 py-md-8 border-top border-bottom border-gray-900-50">
          <div className="row align-items-center">
            <div className="col-12 col-md">
              {/* Heading */}
              <h3 className="font-weight-bold text-white mb-1">
                {_tr("We’re here to help.")}
              </h3>

              {/* Text */}
            </div>
            <div className="col-12 col-md-auto">
              {/* Button */}
              <a href="/contact" className="btn btn-primary lift">
                {_tr("Contact us")}
              </a>
            </div>
          </div>{" "}
          {/* / .row */}
        </div>{" "}
        {/* / .container */}
      </section>
    </>
  );
}
