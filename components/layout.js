import { _tr } from "../services/translate";
import Header from "./header";
import Footer from "./footer";

export default function Layout({ children }) {
  return (
    <div className="pt-md-0">
      <Header />
      {children}
      <Footer />
    </div>
  );
}

