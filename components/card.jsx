export default function Card({ title, description, idx }) {
  return (
    <div className="col-12 col-md-4">
      <div className="row">
        <div className="col-auto col-md-12">
          <div className="row no-gutters align-items-center mb-md-5">
            <div className="col-auto">
              <a
                className="badge bg-success badge-lg badge-rounded-circle badge-primary-soft mt-1"
                href="#!"
              >
                <span>{idx + 1}</span>
              </a>
            </div>
            <div className="col">
              <hr className="d-none d-md-block  mr-n7" />
            </div>
          </div>{" "}
        </div>
        <div className="col col-md-12 ml-n5 ml-md-0">
          <h3>{title}</h3>

          <p className="text-gray-700 mb-6 mb-md-0">{description}</p>
        </div>
      </div>{" "}
    </div>
  );
}
