export default function Card({ name }) {
  return (
    <div className="d-flex">
      <div className="badge bg-success badge-rounded-circle badge-success-soft mt-1 mr-4">
        <i className="fe fe-check"></i>
      </div>

      <p className="text-success">{name}</p>
    </div>
  );
}
