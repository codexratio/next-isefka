import { useState } from "react";
import { _tr } from "../services/translate";
import Link from "next/link";
import { useRouter } from "next/router";

const links = [
  { label: _tr("Consulting"), url: "/consulting" },
  { label: _tr("Contact"), url: "/contact" },
  { label: _tr("Careers"), url: "/careers" },
];

export default function Header() {
  const router = useRouter();
  const [open, setOpen] = useState(false);

  return (
    <nav className="navbar navbar-expand-lg position-sticky w-100 navbar-light bg-light">
      <div className="container-fluid">
        <Link href="/">
          <a className="navbar-brand">Isefka</a>
        </Link>
        <div className="menu-header">
          <ul className={`navbar-nav ml-auto`}>
            <li className="nav-item">
              <Link href="/">
                <a className={`nav-link`} aria-current="page">
                  {_tr("Home")}
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/careers">
                <a className={`nav-link`} aria-current="page">
                  {_tr("Careers")}
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/contact">
                <a className={`nav-link`} aria-current="page">
                  {_tr("Contact")}
                </a>
              </Link>
            </li>
          </ul>
        </div>
        <div>
          {/* <Link href="/" locale="en-US">
            <button
              className={`btn btn-xs mx-2 ${
                router.locale == "en-US" ? "btn-primary" : "btn-outline-primary"
              }`}
            >
              En
            </button>
          </Link>
          <Link href="/" locale="fr">
            <button
              className={`btn btn-xs ${
                router.locale == "fr" ? "btn-primary" : "btn-outline-primary"
              }`}
            >
              Fr
            </button>
          </Link> */}
          <button
            className={`navbar-toggler ${open ? "collapsed" : ""} `}
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03"
            aria-expanded={`${open ? "true" : "false"}`}
            aria-label="Toggle navigation"
            onClick={() => setOpen(!open)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
        <div
          className={`collapse navbar-collapse ${open ? "show" : ""}`}
          id="navbarTogglerDemo03"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link href="/">
                <a className={`nav-link`} aria-current="page">
                  {_tr("Home")}
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/careers">
                <a className={`nav-link`} aria-current="page">
                  {_tr("Careers")}
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/contact">
                <a className={`nav-link`} aria-current="page">
                  {_tr("Contact")}
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
