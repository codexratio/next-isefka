import { _tr } from "../../services/translate";
import Link from "next/link";
import { useState } from "react";

export default function Navbar({ functionsList }) {
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <nav
      id="sidebarMenuDocumentation"
      className="col-md-4 col-lg-3 d-md-block bg-light sidebar collapse"
    >
      <div className="sidebar-sticky pt-3">
        <h1>{_tr("API Reference")} </h1>
        <ul className="nav flex-column">
          {functionsList.map((item, index) => (
            <li
              className="nav-item"
              key={index}
              onClick={() => setActiveIndex(index)}
            >
              <Link href={`/doc/#api-description-${item.nameFunction}`}>
                <a
                  className={`${
                    activeIndex === index ? `nav-link active-link` : `nav-link`
                  }`}
                >
                  <i className="nav-link-icon fa fa-home"></i>
                  <span className="nav-link-text">{item.titleFunction}</span>
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
}
