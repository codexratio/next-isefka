import { _tr } from "../../services/translate";
import Link from "next/link";
import FunctionDetails from "./functionDetails";

export default function Content({
  functionsList,
  hasKey = false,
  userApiKey = ""
}) {
  return (
    <main role="main" className="col-md-8 ml-sm-auto col-lg-9 px-md-4 pt-3">
      <p>{_tr("We offer a REST API to use our services.")}</p>
      <p>{_tr("This documentation contains a description of the API.")}</p>

      <h2 id="copy">{_tr("Getting start")}</h2>

      <ul>
        <li>
          1 {_tr("Get a free API key ")}
          {hasKey ? (
            <Link href="/dashboard">
              {_tr("Your API key is visible in your dashboard")}
            </Link>
          ) : (
            <Link href="/signup">
              {_tr("Sign up here to get your api key")}
            </Link>
          )}
        </li>
        <li>
          2{" "}
          {_tr(
            "Prendre connaissance de cette documentation afin de faire des appels à l'API"
          )}
        </li>
      </ul>
      <hr className="hr-dash my-7" />
      <h2 id="copy">{_tr("API Endpoints")}</h2>
      {functionsList.map((item, index) => (
        <FunctionDetails item={item} key={index} />
      ))}
      <hr className="hr-dash my-7" />
    </main>
  );
}
