import { _tr } from "../../services/translate";
import Link from "next/link";

export default function FunctionDetails({ item, hasApiKey = false }) {
  return (
    <div>
      <h3 id={`api-description-${item.nameFunction}`}>{item.titleFunction}</h3>
      <p>{item.descriptionFunction}</p>
      <span className="badge badge-secondary">POST</span>
      <code>{`https://ikalas.com/api/${item.nameFunction}`}</code>

      <h5>Headers</h5>
      <table className="table table-striped table-sm">
        <thead>
          <tr>
            <th>{_tr("Name")}</th>
            <th>{_tr("Value")}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Content-Type</td>
            <td>
              {item.typeArgument === "file"
                ? "application/x-www-form-urlencoded"
                : "application/json"}
            </td>
          </tr>
          <tr>
            <td>{_tr("Authorization")}</td>
            {hasApiKey ? (
              <td>{userApiKey}</td>
            ) : (
              <td>
                <Link href="/signup">
                  {_tr("Sign up here to get your api key")}
                </Link>
              </td>
            )}
          </tr>
        </tbody>
      </table>

      <h5>Request body</h5>
      <table className="table table-striped table-sm">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Required</th>
            <th>Default value</th>
          </tr>
        </thead>

        {item.FunctionArguments.map((argument, index) =>
          argument.typeArgument !== "system" ? (
            <tr key={index}>
              <td>
                <span className="badge badge-light">
                  <bold>{argument.nameArgument}</bold>
                </span>
              </td>
              <td>{argument.typeArgument}</td>
              <td>{argument.requiredArgument ? _tr("Yes") : _tr("No")}</td>
              <td>{argument.valueArgument}</td>
            </tr>
          ) : null
        )}
      </table>
    </div>
  );
}
