import Layout from "./layout";
import Sidebar from "./sidebar";
import Content from "./content";

export default function Home(props) {
  return (
    <Layout>
      <div className="row mx-0">
        <Sidebar functionsList={props.functionsList} />
        <Content functionsList={props.functionsList} />
      </div>
    </Layout>
  );
};
