import { _tr } from "../services/translate";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";

export default function Footer() {
  const router = useRouter();

  return (
    <div className="pt-11 bg-dark">
      <footer className="py-8 py-md-11 bg-dark">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-4 col-lg-3">
              <strong className="color-white">isefka.</strong>

              <p className="color-white">{_tr("Accessible innovation")}</p>
              <p className="text-gray-700 mb-2">
                {_tr("Follow us on social media")}
              </p>

              <ul className="list-unstyled list-inline list-social mb-6 mb-md-0">
                {/* <li className="list-inline-item list-social-item mr-3">
                  <a
                    href="https://web.facebook.com/Ikalas-106736364289697"
                    className="text-decoration-none"
                  >
                    <Image
                      src="/img/icons/social/facebook.svg"
                      className="rounded-circle"
                      width="40"
                      height="40"
                      unoptimized={true}
                      alt="..."
                    />
                  </a>
                </li> */}
                <li className="list-inline-item list-social-item mr-3">
                  <a
                    href="https://twitter.com/isefka"
                    className="text-decoration-none"
                  >
                    <Image
                      src="/img/icons/social/twitter.svg"
                      width="40"
                      height="40"
                      unoptimized={true}
                      alt="..."
                    />
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-6 col-md-4 col-lg-2">
              <ul className="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
                <li className="mb-3">
                  <a href="/" className="text-reset">
                    {_tr("Home")}
                  </a>
                </li>
                <li className="mb-3">
                  <a href="/careers" className="text-reset">
                    {_tr("Careers")}
                  </a>
                </li>
                <li className="mb-3">
                  <a href="/contact" className="text-reset">
                    {_tr("Contact")}
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
