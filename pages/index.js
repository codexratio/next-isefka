import Head from "next/head";
import Layout from "../components/layout";
import Consulting from "../components/consulting";
import { _tr } from "../services/translate";

export default function Home() {
  return (
    <Layout>
      <Consulting />
    </Layout>
  );
}
