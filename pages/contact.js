import Layout from "../components/layout";
import ApiService from "../services/apiService";
import { _tr } from "../services/translate";
import showToast from "../utils/showToast";
import { useRef } from "react";
import { useOnSubmit } from "../hooks";

export default function Contact() {
  const submitCallback = (event) => {

    let { contactName, contactEmail, contactMessage } = event.target;
    if (!contactName?.value || !contactEmail?.value || !contactMessage?.value) {
      setSubmitedBefore(false);
      return showToast("warning", "Please fill Inputs");
    }
    showToast("info", "Sending message ...");

    ApiService.postContact({
      contactName: contactName.value,
      contactEmail: contactEmail.value,
      contactMessage: contactMessage.value,
    })
      .then(() => {
        if (formRef.current) {
          formRef.current.reset();
        }
        showToast("success", "Done");
      })
      .catch(() => {
        showToast("error", "Message not sent");
        setSubmitedBefore(false);
      });
  };

  const [setSubmitedBefore, handleSubmit] = useOnSubmit(submitCallback);
  const formRef = useRef(null);

  return (
    <Layout>
      <section className="pt-8 pt-md-11 pb-8 pb-md-14">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-10 col-lg-8 text-center">
              <h2 className="font-weight-bold">
                {_tr("Let us hear from you directly")}
              </h2>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-md-12 col-lg-10">
              <form onSubmit={handleSubmit} ref={formRef}>
                <div className="row">
                  <div className="col">
                    <div className="form-group mb-5">
                      <label htmlFor="contactName">{_tr("Full Name")}</label>

                      <input
                        type="text"
                        className="form-control"
                        id="contactName"
                        name="contactName"
                        placeholder={_tr("Full Name")}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group mb-5">
                      <label htmlFor="contactEmail">{_tr("Email")}</label>

                      <input
                        type="email"
                        className="form-control"
                        id="contactEmail"
                        name="contactEmail"
                        placeholder="hello@domain.com"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-group mb-7 mb-md-9">
                      <label htmlFor="contactMessage">
                        {_tr("What can we help you with ?")}
                      </label>

                      <textarea
                        className="form-control"
                        id="contactMessage"
                        name="contactMessage"
                        rows="5"
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="row justify-content-center">
                  <div className="col text-center">
                    <button type="submit" className="btn btn-primary lift">
                      {_tr("Send Message")}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
