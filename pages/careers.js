import Layout from "../components/layout";
import { _tr } from "../services/translate";
import Link from "next/link";

export default function Page() {
  return (
    <Layout>
      <section
        className="py-10 py-md-14 overlay overlay-black overlay-60 bg-cover jarallax"
        style={{ backgroundImage: "url(/img/covers/cover-13.jpg)" }}
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 text-center">
              <h1 className="display-2 text-white">
                {_tr("Want to work with us?")}
              </h1>

              <p className="lead text-white-75 mb-6">
                {_tr(
                  " We're always looking to hire talented folks to join our ever-growing team of designers, developers, and support staff."
                )}
              </p>

              {/* <a href="#!" className="btn btn-primary lift">
                {_tr("View open positions")}
                <i className="fe fe-arrow-right ml-3"></i>
              </a> */}
            </div>
          </div>
        </div>
      </section>

      <div className="position-relative">
        <div className="shape shape-bottom shape-fluid-x svg-shim text-light">
          <svg
            viewBox="0 0 2880 48"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M0 48h2880V0h-720C1442.5 52 720 0 720 0H0v48z"
              fill="currentColor"
            />
          </svg>
        </div>
      </div>

      <section className="pt-8 pt-md-11">
        <div className="container">
          <div className="row align-items-center justify-content-between">
            <div className="col-12 col-md-6">
              <span className="badge bg-primary rounded-pill mb-3">
                <span className="h6 text-uppercase">{_tr("Values")}</span>
              </span>

              <h2>
                {_tr("We care about everyone,")} <br />
                <span className="text-primary-desat">
                  {_tr("but employees are first")}
                </span>
              </h2>

              <p className="font-size-lg text-muted mb-6 mb-md-0">
                {_tr(
                  " No matter your job title or department, if you're a member of our team you are our top priority. We care deeply about everyone who works with us."
                )}
              </p>
            </div>
            <div className="col-12 col-md-6 col-xl-5">
              <div className="row no-gutters">
                <div className="col-4">
                  <div className="w-150 mt-9 p-1 bg-white shadow-lg">
                    <img
                      src="/img/photos/photo-13.jpg"
                      className="img-fluid"
                      alt="..."
                    />
                  </div>
                </div>
                <div className="col-4">
                  <div className="w-150 p-1 bg-white shadow-lg">
                    <img
                      src="/img/photos/photo-14.jpg"
                      className="img-fluid"
                      alt="..."
                    />
                  </div>
                </div>
                <div className="col-4 position-relative">
                  <div className="w-150 mt-11 float-right p-1 bg-white shadow-lg">
                    <img
                      src="/img/photos/photo-15.jpg"
                      className="img-fluid"
                      alt="..."
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-8 py-md-11">
        <div className="container">
          <div className="row align-items-center justify-content-between">
            <div className="col-12 col-md-5 order-md-2">
              <span className="badge bg-success rounded-pill mb-3">
                <span className="h6 text-uppercase">{_tr("Benefits")}</span>
              </span>

              <h2>
                {_tr("We'll take of you with")} <br />
                <span className="text-success">
                  {_tr("a great benefits package")}
                </span>
                .
              </h2>

              {/* <p className="font-size-lg text-muted mb-6 mb-md-0">
                {_tr(
                  " We put our money where our mouthes are. Once you're part of our team, we're going to take the best possible care of you with tons of benefits and perks unavailable anywhere else."
                )}{" "}
              </p> */}
            </div>
            <div className="col-12 col-md-6 order-md-1">
              <div className="card card-border border-success shadow-lg">
                <div className="card-body">
                  <div className="list-group list-group-flush">
                    <div className="list-group-item d-flex align-items-center">
                      <div className="mr-auto">
                        <p className="font-weight-bold mb-1">
                          {_tr("Agile method")}{" "}
                        </p>

                        <p className="font-size-sm text-muted mb-0">
                          {_tr(" We work with an agile methodology.")}{" "}
                        </p>
                      </div>

                      <div className="badge badge-rounded-circle bg-success ml-4">
                        <i className="fe fe-check"></i>
                      </div>
                    </div>
                    <div className="list-group-item d-flex align-items-center">
                      <div className="mr-auto">
                        <p className="font-weight-bold mb-1">
                          {_tr(" Remote")}{" "}
                        </p>

                        <p className="font-size-sm text-muted mb-0">
                          {_tr(" Work where you want.")}{" "}
                        </p>
                      </div>

                      <div className="badge badge-rounded-circle bg-success ml-4">
                        <i className="fe fe-check"></i>
                      </div>
                    </div>
                    <div className="list-group-item d-flex align-items-center">
                      <div className="mr-auto">
                        <p className="font-weight-bold mb-1">
                          {_tr(" Competitive salary")}{" "}
                        </p>

                        <p className="font-size-sm text-muted mb-0">
                          {_tr(" We offer the best salaries on the market.")}{" "}
                        </p>
                      </div>

                      <div className="badge badge-rounded-circle bg-success ml-4">
                        <i className="fe fe-check"></i>
                      </div>
                    </div>
                    <div className="list-group-item d-flex align-items-center">
                      <div className="mr-auto">
                        <p className="font-weight-bold mb-1">
                          {_tr(" Efficiency first")}{" "}
                        </p>

                        <p className="font-size-sm text-muted mb-0">
                          {_tr(
                            " Our working methods are efficiency oriented. No endless meetings, no restrictive schedules."
                          )}{" "}
                        </p>
                      </div>

                      <div className="badge badge-rounded-circle bg-success ml-4">
                        <i className="fe fe-check"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="position-relative">
        <div className="shape shape-top shape-fluid-x svg-shim text-light">
          <svg
            viewBox="0 0 2880 250"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M0 0h2880v125h-720L720 250H0V0z" fill="currentColor" />
          </svg>
        </div>
      </div>
      <section
        className="py-14 py-lg-16 bg-cover jarallax"
        style={{ backgroundImage: "url(img/covers/cover-4.jpg)" }}
      ></section>
      <div className="position-relative">
        <div className="shape shape-bottom shape-fluid-x svg-shim text-light">
          <svg
            viewBox="0 0 2880 250"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M720 125L2160 0h720v250H0V125h720z" fill="currentColor" />
          </svg>
        </div>
      </div>

      <section className="pt-6 pt-md-8">
        <div className="container pb-8 pb-md-11 border-bottom border-gray-300">
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 text-center">
              <span className="badge rounded-pill badge-primary-desat-soft mb-3">
                <span className="h6 text-uppercase">{_tr("Applying")}</span>
              </span>

              <h2>
                {_tr("Let’s find you an")}{" "}
                <span className="text-primary"> {_tr("open position")}</span>.
              </h2>

              <p className="font-size-lg text-gray-700 mb-7 mb-md-9">
                {_tr(
                  " Find the right job for you no matter what it is that you do."
                )}
              </p>
            </div>
          </div>
          {/* <div className="row">
            <div className="col-12">
              <form className="mb-7 mb-md-9">
                <div className="row">
                  <div className="col-12 col-md-4">
                    <div className="form-group mb-5 mb-md-0">
                      <label htmlFor="applyRoles">{_tr("Roles")}</label>

                      <select
                        id="applyRoles"
                        className="form-select"
                        defaultChecked="all"
                      >
                        <option value="all">{_tr("All roles")}</option>
                        <option value="design">{_tr("Design")}</option>
                        <option value="engineering">
                          {_tr("Engineering")}
                        </option>
                        <option value="product">{_tr("Product")}</option>
                        <option value="testing">{_tr("Testing")}</option>
                        <option value="support">{_tr("Support")}</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-12 col-md-4">
                    <div className="form-group mb-5 mb-md-0">
                      <label htmlFor="applyTeam">{_tr("Team")}</label>

                      <select
                        id="applyTeam"
                        className="form-select"
                        defaultChecked="all"
                      >
                        <option value="all">{_tr("All teams")}</option>
                        <option value="consumer">{_tr("Consumer")}</option>
                        <option value="consulting">{_tr("Consulting")}</option>
                        <option value="tools">{_tr("Internal tools")}</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-12 col-md-4">
                    <div className="form-group mb-0">
                      <label htmlFor="applyLocation">{_tr("Location")}</label>

                      <select
                        id="applyLocation"
                        className="form-select"
                        defaultChecked="all"
                      >
                        <option value="all">{_tr("All locations")}</option>
                        <option value="london">{_tr("London")}</option>
                        <option value="la">{_tr("Los Angeles")}</option>
                        <option value="paris">{_tr("Paris")}</option>
                        <option value="sf">{_tr("San Francisco")}</option>
                      </select>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div> */}
          {/* <div className="row align-items-center mb-5">
            <div className="col">
              <h4 className="font-weight-bold mb-1">{_tr("Design & UX")}</h4>

              <p className="font-size-sm text-muted mb-0">
                {_tr(
                  " User experience and design are top priorities at Landkit."
                )}{" "}
              </p>
            </div>
            <div className="col-auto">
              <span className="badge rounded-pill bg-success">
                <span className="h6 text-uppercase">{_tr("4 openings")}</span>
              </span>
            </div>
          </div> */}
          {/* <div className="row">
            <div className="col-12">
              <div className="table-responsive mb-7 mb-md-9">
                <table className="table table-align-middle">
                  <thead>
                    <tr>
                      <th>
                        <span className="h6 text-uppercase font-weight-bold">
                          {_tr("Role")}
                        </span>
                      </th>
                      <th>
                        <span className="h6 text-uppercase font-weight-bold">
                          {_tr("Team")}
                        </span>
                      </th>
                      <th>
                        <span className="h6 text-uppercase font-weight-bold">
                          {_tr("Location")}
                        </span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="mb-1">{_tr("Senior UX Designer")}</p>
                          <p className="font-size-sm text-muted mb-0">
                            {_tr(
                              "Responsible for design systems and brand management."
                            )}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">{_tr("Consumer")}</p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">
                            {_tr("Los Angeles")}
                          </p>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="mb-1">{_tr("Motion Designer")}</p>
                          <p className="font-size-sm text-muted mb-0">
                            {_tr(" Responsible for creating life in our apps.")}{" "}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">{_tr("Product")}</p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">
                            {_tr("San Francisco, CA")}
                          </p>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="mb-1">{_tr("Design Researcher")}</p>
                          <p className="font-size-sm text-muted mb-0">
                            {_tr(
                              "Help us make the best decisions with qualitative experiments."
                            )}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">
                            {_tr("Consulting")}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">{_tr("London")}</p>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="mb-1">{_tr("Production Designer")}</p>
                          <p className="font-size-sm text-muted mb-0">
                            {_tr(
                              "Create, collect, and distribute beautiful assets."
                            )}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">
                            {_tr("Consulting")}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a
                          href="career-single.html"
                          className="text-reset text-decoration-none"
                        >
                          <p className="font-size-sm mb-0">{_tr("Paris")}</p>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div> */}
          <div className="row align-items-center mb-5">
            <div className="col">
              <h4 className="font-weight-bold mb-1">
                {_tr("Software development")}
              </h4>

              <p className="font-size-sm text-muted mb-0">
                {/* {_tr(
                  "This is Landkit's bread and butter – help us make it better."
                )} */}
              </p>
            </div>
            <div className="col-auto">
              <span className="badge rounded-pill bg-success">
                <span className="h6 text-uppercase">{_tr("1 opening")}</span>
              </span>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="table-responsive mb-5">
                <table className="table table-align-middle">
                  <thead>
                    <tr>
                      <th>
                        <span className="h6 text-uppercase font-weight-bold">
                          {_tr(" Role")}{" "}
                        </span>
                      </th>
                      <th>
                        <span className="h6 text-uppercase font-weight-bold">
                          {_tr(" Team")}{" "}
                        </span>
                      </th>
                      <th>
                        <span className="h6 text-uppercase font-weight-bold">
                          {_tr(" Location")}{" "}
                        </span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <a href="#" className="text-reset text-decoration-none">
                          <p className="mb-1">
                            {_tr(".NET&React Fullstack developer")}
                          </p>
                          <p className="font-size-sm text-muted mb-0">
                            {_tr(
                              "Develop back-end applications with .NET and frontend applications with React"
                            )}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a href="#" className="text-reset text-decoration-none">
                          <p className="font-size-sm mb-0">
                            {_tr("Engeneering")}
                          </p>
                        </a>
                      </td>
                      <td>
                        <a href="#" className="text-reset text-decoration-none">
                          <p className="font-size-sm mb-0">{_tr("Remote")}</p>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <p className="font-size-sm text-center text-muted mb-0">
                {_tr("Don’t see the job you want ? ")}
                <Link href="/contact">{_tr("Let us know")}</Link>.
              </p>
            </div>
          </div>
        </div>
      </section>
      <section className="pt-8 pt-md-11">
        <div className="container">
          <div className="card card-row shadow-light-lg">
            <div className="row no-gutters">
              <a
                className="col-12 col-md-6 bg-cover card-img-left"
                style={{ backgroundImage: "url(/img/photos/photo-1.jpg)" }}
              >
                <img
                  src="/img/photos/photo-1.jpg"
                  alt="..."
                  className="img-fluid d-md-none invisible"
                />

                <div className="shape shape-right shape-fluid-y svg-shim text-white d-none d-md-block">
                  <svg
                    viewBox="0 0 112 690"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M116 0H51v172C76 384 0 517 0 517v173h116V0z"
                      fill="currentColor"
                    />
                  </svg>
                </div>
              </a>
              <div className="col-12 col-md-6 order-md-1">
                <div className="card-body">
                  <blockquote className="blockquote mb-0 text-center">
                    <p className="mb-5 mb-md-7">
                      “
                      {_tr(
                        "No matter your job title or department, if you're a member of our team you are our top priority. We care deeply about everyone who works with us."
                      )}
                      “
                    </p>

                    <footer className="blockquote-footer">
                      <span className="h6 text-uppercase">
                        {_tr("ISEFKA TEAM")}
                      </span>
                    </footer>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="position-relative mt-n11">
        <div className="shape shape-bottom shape-fluid-x svg-shim text-dark">
          <svg
            viewBox="0 0 2880 48"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M0 48h2880V0h-720C1442.5 52 720 0 720 0H0v48z"
              fill="currentColor"
            />
          </svg>
        </div>
      </div>
    </Layout>
  );
}
