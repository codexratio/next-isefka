import { useState } from "react";

const useOnSubmit = (cb) => {
  const [submitedBefore, setSubmitedBefore] = useState(false);

  return [
    setSubmitedBefore,
    (e) => {
      e.preventDefault();
      if (!submitedBefore) {
        setSubmitedBefore(true);
        cb(e);
      }
    },
  ];
};

export default useOnSubmit;
